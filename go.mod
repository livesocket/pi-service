module gitlab.com/livesocket/pi-service

go 1.12

require (
	github.com/gammazero/nexus/v3 v3.0.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/jmoiron/sqlx v1.2.0
	gitlab.com/livesocket/conv v0.0.0-20191012101209-727e1c03a95c
	gitlab.com/livesocket/service v1.4.3
	golang.org/x/crypto v0.0.0-20191108234033-bd318be0434a
	golang.org/x/net v0.0.0-20191109021931-daa7c04131f5 // indirect
	golang.org/x/sys v0.0.0-20191110163157-d32e6e3b99c4 // indirect
	golang.org/x/tools v0.0.0-20191109212701-97ad0ed33101 // indirect
	golang.org/x/xerrors v0.0.0-20191011141410-1b5146add898 // indirect
)
