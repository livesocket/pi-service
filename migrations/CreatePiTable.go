package migrations

import "github.com/jmoiron/sqlx"

func CreatePiTable(tx *sqlx.Tx) error {
	_, err := tx.Exec("CREATE TABLE `pi` (`id` varchar(255) NOT NULL, `channel` varchar(255) NOT NULL,`project` varchar(255) NOT NULL,`password` varchar(255) NOT NULL,`notes` text NOT NULL, `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY(`id`))")
	if err != nil {
		return err
	}
	return nil
}
