package util

import (
	"log"

	"golang.org/x/crypto/bcrypt"
)

// HashAndSalt Hash and salt a plain text password
func HashAndSalt(pword string) string {
	hash, err := bcrypt.GenerateFromPassword([]byte(pword), bcrypt.DefaultCost)
	if err != nil {
		log.Println(err)
	}
	return string(hash)
}

// ComparePasswords compare a hashed and plain text password
func ComparePasswords(hashed string, plain string) bool {
	byteHash := []byte(hashed)
	err := bcrypt.CompareHashAndPassword(byteHash, []byte(plain))
	if err != nil {
		log.Println(err)
		return false
	}

	return true
}
