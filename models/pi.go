package models

import (
	"database/sql"
	"time"

	"gitlab.com/livesocket/service"
)

type Pi struct {
	ID        string     `db:"id" json:"id"`
	Channel   string     `db:"channel" json:"channel"`
	Project   string     `db:"project" json:"project"`
	Notes     string     `db:"notes" json:"notes"`
	Timestamp *time.Time `db:"timestamp" json:"timestamp"`
}

type DbPi struct {
	ID        string     `db:"id" json:"id"`
	Channel   string     `db:"channel" json:"channel"`
	Project   string     `db:"project" json:"project"`
	Password  string     `db:"password" json:"password"`
	Notes     string     `db:"notes" json:"notes"`
	Timestamp *time.Time `db:"timestamp" json:"timestamp"`
}

func NewPi(id, channel, project, password string, timestamp time.Time) *Pi {
	now := time.Now()
	return &Pi{
		ID:        id,
		Channel:   channel,
		Project:   project,
		Notes:     "",
		Timestamp: &now,
	}
}

func FindPi(id string) (*Pi, error) {
	pi := Pi{}
	err := service.DB.Get(&pi, "SELECT * FROM `pi` WHERE `id`=?", id)
	return &pi, err
}

func AllPi() ([]Pi, error) {
	pis := []Pi{}
	err := service.DB.Select(&pis, "SELECT `id`,`channel`,`project`,`notes`,`timestamp` FROM `pi`")
	return pis, err
}

func FindPiPassword(id string) (string, error) {
	pword := ""
	err := service.DB.Get(&pword, "SELECT `password` FROM `pi` WHERE `id`=?", id)
	return pword, err
}

func SavePing(id string) (sql.Result, error) {
	return service.DB.Exec("UPDATE `pi` SET `timestamp`=CURRENT_TIMESTAMP WHERE `id`=?", id)
}

func (pi *Pi) Create(password string) (sql.Result, error) {
	return service.DB.Exec("INSERT INTO `pi` (`id`,`channel`,`project`,`password`,`notes`,`timestamp`) VALUES (?,?,?,?,?,CURRENT_TIMESTAMP)", pi.ID, pi.Channel, pi.Project, password, pi.Notes, pi.Timestamp)
}

func (pi *Pi) Update() (sql.Result, error) {
	return service.DB.NamedExec("UPDATE `pi` SET `channel`=:channel,`project`=:project,`notes`=:notes,`timestamp`=:timestamp WHERE `id`=:id", pi)
}
