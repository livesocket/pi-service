package main

import (
	"log"
	"os"
	"os/signal"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/livesocket/pi-service/actions"
	"gitlab.com/livesocket/pi-service/migrations"
	"gitlab.com/livesocket/service"
	"gitlab.com/livesocket/service/lib"
)

func main() {
	close, err := service.NewStandardService([]lib.Action{
		actions.AuthenticateAction,
		actions.ReportAction,
		actions.GetAction,
	}, nil, "__pi_service", migrations.CreatePiTable)
	defer close()
	if err != nil {
		panic(err)
	}

	// Wait for CTRL-c or client close while handling events.
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	select {
	case <-sigChan:
	case <-service.Socket.Client.Done():
		log.Print("Router gone, exiting")
		return
	}
}
