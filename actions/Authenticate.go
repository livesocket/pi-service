package actions

import (
	"errors"

	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/conv"
	"gitlab.com/livesocket/pi-service/models"
	"gitlab.com/livesocket/service/lib"
)

// AuthenticateAction Authenticates a login from a Pi
//
// private.pi.authenticate
// {username string, password string}
//
// Returns nothing or error
var AuthenticateAction = lib.Action{
	Proc:    "private.pi.authenticate",
	Handler: authenticate,
}

type authenticateInput struct {
	Username string
	Password string
}

func authenticate(invocation *wamp.Invocation) client.InvokeResult {
	// Get input args from call
	input, err := getAuthenticateInput(invocation.ArgumentsKw)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Get the hashed password
	hashed, err := models.FindPiPassword(input.Username)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// TEMP Check password is valid
	if hashed != input.Password {
		return lib.ErrorResult(errors.New("incorrect password"))
	}

	// Check password is valid
	// ok := util.ComparePasswords(hashed, input.Password)
	// if !ok {
	// 	return lib.ErrorResult(errors.New("incorrect password"))
	// }

	// Return result
	return client.InvokeResult{}
}

func getAuthenticateInput(kwargs wamp.Dict) (*authenticateInput, error) {
	if kwargs == nil {
		return nil, errors.New("Missing Kwargs")
	}

	if kwargs["username"] == nil {
		return nil, errors.New("Missing username")
	}

	if kwargs["password"] == nil {
		return nil, errors.New("Missing password")
	}

	return &authenticateInput{
		Username: conv.ToString(kwargs["username"]),
		Password: conv.ToString(kwargs["password"]),
	}, nil
}
