package actions

import (
	"errors"

	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/conv"
	"gitlab.com/livesocket/pi-service/models"
	"gitlab.com/livesocket/service/lib"
)

// ReportAction Report a pi as alive
//
// pi.report
// [id string]
//
// Returns nothing or error
var ReportAction = lib.Action{
	Proc:    "pi.report",
	Handler: report,
}

type reportInput struct {
	ID string
}

func report(invocation *wamp.Invocation) client.InvokeResult {
	// Get input args from call
	input, err := getReportInput(invocation.Arguments)
	if err != nil {
		return lib.ErrorResult(err)
	}

	_, err = models.SavePing(input.ID)
	if err != nil {
		return lib.ErrorResult(err)
	}

	// Return result
	return client.InvokeResult{}
}

func getReportInput(args wamp.List) (*reportInput, error) {
	if len(args) == 0 {
		return nil, errors.New("Missing Pi ID")
	}

	return &reportInput{
		ID: conv.ToString(args[0]),
	}, nil
}
