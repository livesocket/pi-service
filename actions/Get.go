package actions

import (
	"github.com/gammazero/nexus/v3/client"
	"github.com/gammazero/nexus/v3/wamp"
	"gitlab.com/livesocket/pi-service/models"
	"gitlab.com/livesocket/service/lib"
)

// GetAction Get a list of registered pis
//
// admin.pi.get
//
// Returns nothing or error
var GetAction = lib.Action{
	Proc:    "admin.pi.get",
	Handler: get,
}

func get(invocation *wamp.Invocation) client.InvokeResult {
	// Find the matching pi
	pis, err := models.AllPi()
	if err != nil {
		return lib.ErrorResult(err)
	}

	list := wamp.List{}
	for _, pi := range pis {
		list = append(list, pi)
	}

	// Return result
	return client.InvokeResult{Args: list}
}
