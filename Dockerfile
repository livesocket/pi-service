FROM golang:alpine as base
RUN apk --no-cache add git ca-certificates g++
WORKDIR /repos/pi-service
ADD go.mod go.sum ./
RUN go mod download

FROM base as builder
WORKDIR /repos/pi-service
ADD . .
RUN CGO_ENABLED=0 GOOS=linux go build -o pi-service

FROM scratch as release
COPY --from=builder /repos/pi-service/pi-service /pi-service
EXPOSE 8080
ENTRYPOINT ["/pi-service"]

